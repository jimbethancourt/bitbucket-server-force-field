package com.carolynvs.stash.plugin.force_field;

import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageUtils;
import java.util.Map;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PreventForcePushHook implements PreReceiveRepositoryHook
{
    private final CommitService commitService;
    private final I18nService i18nService;
    private final PathMatcher pathMater = new AntPathMatcher();

    public PreventForcePushHook(CommitService commitService, I18nService i18nService)
    {
        this.commitService = commitService;
        this.i18nService = i18nService;
    }

    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse response)
    {
        List<String> protectedRefs = getProtectedRefs(context);
        Repository repository = context.getRepository();

        List<String> blockedRefs = identifyReferencesWithBlockedChangesets(refChanges, protectedRefs, repository);

        if (!blockedRefs.isEmpty())
        {
            printError(response, blockedRefs);
        }

        return blockedRefs.isEmpty();
    }

    List<String> getProtectedRefs(@Nonnull RepositoryHookContext context)
    {
        ArrayList<String> protectedRefs = new ArrayList<String>();

        Settings settings = context.getSettings();
        Map<String, Object> settingsMap = settings.asMap();
        String references = settings.getString("references");
        if(settingsMap.containsKey("references") && null !=references && !references.isEmpty()) {
            String[] refs = references.split(" ");
            for(String ref : refs)
            {
                ref = ReferenceCleaner.fixUnmatchableRegex(ref);
                protectedRefs.add(ref);
            }
        }
        return protectedRefs;
    }

    private List<String> identifyReferencesWithBlockedChangesets(Collection<RefChange> refChanges, List<String> protectedRefs, Repository repository)
    {
        List<String> blocked = new ArrayList<String>();
        for(RefChange refChange : refChanges)
        {
            String refId = refChange.getRefId();
            if(isProtectedRef(protectedRefs, refChange) && isForcePush(repository, refChange))
            {
                blocked.add(refId);
            }
        }
        return blocked;
    }

    private void printError(HookResponse response, List<String> blockedRefs)
    {
        response.out().println("=================================");
        response.out().println(i18nService.getText("force-field.plugin-name", "Force Field"));
        for (String refId : blockedRefs)
        {
            response.out().println(i18nService.getText("force-field.error-message", "The repository administrator has disabled force pushes to {0}", refId));
        }
        response.out().println("=================================");
    }

    private boolean isProtectedRef(List<String> restrictedRefs, RefChange refChange)
    {
        String refId = refChange.getRefId();
        for(String refPattern : restrictedRefs)
        {
            if(pathMater.match(refPattern, refId))
            {
                return true;
            }
        }

        return false;
    }

    private boolean isForcePush(Repository repository, RefChange refChange)
    {
        boolean isReplacingChangesets = refChange.getType() == RefChangeType.UPDATE;
        if(!isReplacingChangesets)
            return false;

        Page<Commit> changes = getChangesets(repository, refChange);
        return changes.getSize() > 0;
    }

    private Page<Commit> getChangesets(Repository repository, RefChange refChange)
    {
        CommitsBetweenRequest request = new CommitsBetweenRequest.Builder(repository)
                .exclude(refChange.getToHash())
                .include(refChange.getFromHash())
                .build();

        return commitService.getCommitsBetween(request, PageUtils.newRequest(0, 1));
    }
}
